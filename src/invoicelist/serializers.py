from rest_framework import serializers
from .models import SiteConfiguration,Customer,InvoiceLine,TaxRate,Invoice

class SiteConfigurationSerializer(serializers.ModelSerializer):
    class Meta:
        model = SiteConfiguration
        fields = ('first_name', 'last_name', 'company', 'address_street','postalcode','city','country','vat_tax_id','phone_number')


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = ('first_name', 'last_name', 'company', 'address_street','postalcode','city','country','vat_tax_id','phone_number')


class InvoiceLineSerializer(serializers.ModelSerializer):
    class Meta:
        model = InvoiceLine
        fields = ('item','description','quantity','single_value','invoice','line_total')

class TaxRateSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaxRate
        fields = ('name','rate')

class InvoiceSerializer(serializers.ModelSerializer):
    vat_tax_rate = serializers.PrimaryKeyRelatedField(queryset=TaxRate.objects.all())
    customer = serializers.PrimaryKeyRelatedField(queryset=Customer.objects.all())
    invoice_date = serializers.DateTimeField()
    due_date = serializers.DateTimeField()
    class Meta:
        model = Invoice
        fields = ('invoice_number','invoice_number_prefix','invoice_date','due_date','amount','vat_tax_rate','customer',)
