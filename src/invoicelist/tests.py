from django.test import TestCase
from .models import SiteConfiguration, Person, Customer,InvoiceLine, Invoice, TaxRate
from .views import SiteConfigurationAPIView,CustomerViewSet,InvoiceViewSet,InvoiceLineViewSet
from rest_framework.test import APIRequestFactory
from rest_framework.test import force_authenticate
from django.contrib.auth.models import User
import datetime

class SiteConfigurationTestCase(TestCase):
    def setUp(self):
        SiteConfiguration.objects.create(
            first_name="Michael",
            last_name="Schmidt",
            company="Webkrafts",
            address_street="Test Str. 71",
            postalcode="10998",
            city="Berlin",
            country="Germany",
            vat_tax_id="DE123456789",
            phone_number="+49232323232"
            )

    def test_create_person(self):
        with self.assertRaises(AttributeError) as cm:
            Person.objects.create(
                first_name="Michael",
                last_name="Schmidt",
                company="Webkrafts",
                address_street="Test Str. 71",
                postalcode="10998",
                city="Berlin",
                country="Germany",
                vat_tax_id="DE123456789",
                email="mm@tt.com",
                phone_number="+49232323232"
                )

    def test_createSiteConfiguration(self):
        config = SiteConfiguration.objects.get()
        self.assertEqual(config.first_name,"Michael")
        self.assertEqual(config.last_name,"Schmidt")
        self.assertEqual(config.company,"Webkrafts")
        self.assertEqual(config.address_street,"Test Str. 71")
        self.assertEqual(config.postalcode,"10998")
        self.assertEqual(config.city,"Berlin")
        self.assertEqual(config.country,"Germany")
        self.assertEqual(config.vat_tax_id,"DE123456789")
        self.assertEqual(config.phone_number,"+49232323232")

    def test_SiteConfiguration_logged_in(self):
        factory = APIRequestFactory()
        user =  User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        request = factory.get('/config/')
        force_authenticate(request, user=user)
        view = SiteConfigurationAPIView.as_view()
        response = view(request)
        self.assertEqual(response.data, {
            'first_name': 'Michael',
            'last_name' : "Schmidt",
            'company': 'Webkrafts',
            'address_street':'Test Str. 71',
            'postalcode':'10998',
            'city':'Berlin',
            'country':'Germany',
            'vat_tax_id':'DE123456789',
            'phone_number':'+49232323232',
            })

    def test_SiteConfiguration_logged_in_change(self):
        factory = APIRequestFactory()
        user =  User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        request = factory.put('/config/', {
            'first_name': 'Michael2',
            'last_name' : "Schmidt",
            'company': 'Webkrafts',
            'address_street':'Test Str. 71',
            'postalcode':'10998',
            'city':'Berlin',
            'country':'Germany',
            'vat_tax_id':'DE123456789',
            'phone_number':'+49232323232',
            })
        force_authenticate(request, user=user)
        view = SiteConfigurationAPIView.as_view()
        response = view(request)
        request = factory.get('/config/')
        force_authenticate(request, user=user)
        response = view(request)
        self.assertEqual(response.data, {
            'first_name': 'Michael2',
            'last_name' : "Schmidt",
            'company': 'Webkrafts',
            'address_street':'Test Str. 71',
            'postalcode':'10998',
            'city':'Berlin',
            'country':'Germany',
            'vat_tax_id':'DE123456789',
            'phone_number':'+49232323232',
            })
    def test_SiteConfiguration_not_logged_in(self):
        factory = APIRequestFactory()
        request = factory.get('/config/')
        view = SiteConfigurationAPIView.as_view()
        response = view(request)
        self.assertNotEqual(response.data, {
            'first_name': 'Michael',
            'last_name' : "Schmidt",
            'company': 'Webkrafts',
            'address_street':'Test Str. 71',
            'postalcode':'10998',
            'city':'Berlin',
            'country':'Germany',
            'vat_tax_id':'DE123456789',
            'phone_number':'+49232323232',
            })

class CustomerTestCase(TestCase):
    def setUp(self):
        Customer.objects.create(
            first_name="Michael",
            last_name="Schmidt",
            company="Webkrafts",
            address_street="Test Str. 71",
            postalcode="10998",
            city="Berlin",
            country="Germany",
            vat_tax_id="DE123456789",
            phone_number="+49232323232"
            )
        Customer.objects.create(
            first_name="Michael 2",
            last_name="Schmidt 2",
            company="Webkrafts",
            address_street="Test Str. 72",
            postalcode="10998",
            city="Berlin",
            country="Germany",
            vat_tax_id="DE123456789",
            phone_number="+49232323232"
            )

    def test_create_customer(self):
        customer = Customer.objects.create(
            first_name="Michael 2",
            last_name="Schmidt 2",
            company="Webkrafts",
            address_street="Test Str. 72",
            postalcode="10998",
            city="Berlin",
            country="Germany",
            vat_tax_id="DE123456789",
            phone_number="+49232323232"
            )
        self.assertEqual(customer.postalcode,"10998")
        self.assertNotEqual(customer.phone_number,"1099845363")

    def test_get_customer_logged_in(self):
        factory = APIRequestFactory()
        user =  User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        request = factory.get('/customer/')
        force_authenticate(request, user=user)
        view = CustomerViewSet.as_view({'get':'list'})
        response = view(request)
        self.assertEqual(response.data, [{
            'first_name': 'Michael',
            'last_name' : "Schmidt",
            'company': 'Webkrafts',
            'address_street':'Test Str. 71',
            'postalcode':'10998',
            'city':'Berlin',
            'country':'Germany',
            'vat_tax_id':'DE123456789',
            'phone_number':'+49232323232',
            },
            {
            'first_name':"Michael 2",
            'last_name':"Schmidt 2",
            'company':"Webkrafts",
            'address_street':"Test Str. 72",
            'postalcode':"10998",
            'city':"Berlin",
            'country':"Germany",
            'vat_tax_id':"DE123456789",
            'phone_number':"+49232323232"
            }])

    def test_get_customer_not_logged_in(self):
        factory = APIRequestFactory()
        request = factory.get('/customer/')
        view = CustomerViewSet.as_view({'get':'list'})
        response = view(request)
        self.assertNotEqual(response.data, [{
            'first_name': 'Michael',
            'last_name' : "Schmidt",
            'company': 'Webkrafts',
            'address_street':'Test Str. 71',
            'postalcode':'10998',
            'city':'Berlin',
            'country':'Germany',
            'vat_tax_id':'DE123456789',
            'phone_number':'+49232323232',
            },
            {
            'first_name':"Michael 2",
            'last_name':"Schmidt 2",
            'company':"Webkrafts",
            'address_street':"Test Str. 72",
            'postalcode':"10998",
            'city':"Berlin",
            'country':"Germany",
            'vat_tax_id':"DE123456789",
            'phone_number':"+49232323232"
            }])


    def test_logged_in_change_customer(self):
        factory = APIRequestFactory()
        user =  User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        customer_to_change = Customer.objects.first()
        request = factory.put('/customer/', {
            'first_name': 'Pepe',
            'last_name' : "Sanchez",
            'company': 'Webkrafts',
            'address_street':'Test Str. 71',
            'postalcode':'10998',
            'city':'Berlin',
            'country':'Germany',
            'vat_tax_id':'DE123456789',
            'phone_number':'+49232323232',
            })
        force_authenticate(request, user=user)
        view = CustomerViewSet.as_view({'put': 'update'})
        response = view(request,pk=customer_to_change.pk)
        request = factory.get('/customer/1')
        force_authenticate(request, user=user)
        view = CustomerViewSet.as_view({'get': 'retrieve'})
        response = view(request,pk=customer_to_change.pk)

        self.assertEqual(response.data, {
            'first_name': 'Pepe',
            'last_name' : "Sanchez",
            'company': 'Webkrafts',
            'address_street':'Test Str. 71',
            'postalcode':'10998',
            'city':'Berlin',
            'country':'Germany',
            'vat_tax_id':'DE123456789',
            'phone_number':'+49232323232',
            })


    def test_not_logged_in_change_customer(self):
        factory = APIRequestFactory()
        customer_to_change = Customer.objects.first()
        request = factory.put('/customer/8', {
            'first_name': 'Pepe',
            'last_name' : "Sanchez",
            'company': 'Webkrafts',
            'address_street':'Test Str. 71',
            'postalcode':'10998',
            'city':'Berlin',
            'country':'Germany',
            'vat_tax_id':'DE123456789',
            'phone_number':'+49232323232',
            })
        view = CustomerViewSet.as_view({'put': 'update'})
        response = view(request,pk=customer_to_change.pk)
        self.assertEqual(response.status_code,403)



class InvoiceLineAndInvoiceTestCase(TestCase):
    def setUp(self):
        customer = Customer.objects.create(
            first_name="Michael 2",
            last_name="Schmidt 2",
            company="Webkrafts",
            address_street="Test Str. 72",
            postalcode="10998",
            city="Berlin",
            country="Germany",
            vat_tax_id="DE123456789",
            phone_number="+49232323232"
            )

        tax_rate  = TaxRate.objects.create(
            name="MwSt.",
            rate=0.19,)

        invoice = Invoice.objects.create(
            invoice_number=1000,
            invoice_number_prefix="2015",
            invoice_date=datetime.datetime(2015,9,22),
            customer=customer,
            amount=1000,
            due_date=datetime.datetime(2015,10,22),
            vat_tax_rate=tax_rate,
            )

        InvoiceLine.objects.create(
            item="This is a test item", 
            description="we installed the test item in the customer premises",
            quantity=23 ,
            single_value=1000,
            invoice = invoice
            )

        InvoiceLine.objects.create(
            item="This is a test item 2",  
            description="we installed the test item 2 in the customer premises",
            quantity=1, 
            single_value=500,
            invoice = invoice
            )
    
    def test_logged_get_invoice(self):
        factory = APIRequestFactory()
        user =  User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        request = factory.get('/invoice/')
        force_authenticate(request, user=user)
        view = InvoiceViewSet.as_view({'get':'list'})
        response = view(request)
        self.assertEqual(response.status_code,200)

    def test_logged_create_invoice(self):
        factory = APIRequestFactory()
        user =  User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        customer = Customer.objects.first()
        tax_rate = TaxRate.objects.first()
        request = factory.post('/invoice/',{
            'invoice_number': 100,
            'invoice_number_prefix':'Test Prefix',
            'invoice_date': datetime.datetime(2015,9,1),
            'customer':customer.pk,
            'amount':'100.55',
            'due_date':datetime.datetime(2015,10,21),
            'vat_tax_rate':tax_rate.pk,
            },format='json')
        force_authenticate(request, user=user)
        view = InvoiceViewSet.as_view({'post':'create'})
        response = view(request)
        self.assertEqual(response.status_code,201) 
        self.assertEqual(response.data,{
            'invoice_number_prefix': 'Test Prefix', 
            'due_date': '2015-10-21T00:00:00Z', 
            'amount': '100.55', 
            'customer': customer.pk, 
            'invoice_number': 100, 
            'invoice_date': '2015-09-01T00:00:00Z', 
            'vat_tax_rate': tax_rate.pk,
        })
    
    def test_logged_update_invoice(self):
        factory = APIRequestFactory()
        user =  User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        invoice = Invoice.objects.first()
        customer = invoice.customer
        tax_rate = invoice.vat_tax_rate
        request = factory.put('/invoice/',{
            'invoice_number': 100,
            'invoice_number_prefix':'Updated Invoice',
            'invoice_date': datetime.datetime(2015,9,2),
            'customer':customer.pk,
            'amount':'100.55',
            'due_date':datetime.datetime(2015,10,14),
            'vat_tax_rate':tax_rate.pk,
            },format='json')
        force_authenticate(request, user=user)
        view = InvoiceViewSet.as_view({'put':'update'})
        response = view(request,pk=invoice.pk)
        self.assertEqual(response.data,{
            'invoice_date': '2015-09-02T00:00:00Z', 
            'amount': '100.55', 
            'invoice_number': 100, 
            'customer': customer.pk, 
            'vat_tax_rate': tax_rate.pk, 
            'due_date': '2015-10-14T00:00:00Z', 
            'invoice_number_prefix': 'Updated Invoice',
            })

    def test_logged_invoice_line_create(self):
        factory = APIRequestFactory()
        user =  User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        invoice = Invoice.objects.first()
        request = factory.post('/invoice_line/',{
                'item':"test Item",
                'description':"test Description",
                'quantity':20,
                'single_value':22.55,
                'invoice':invoice.pk,
            })
        force_authenticate(request, user=user)
        view = InvoiceLineViewSet.as_view({'post':'create'})
        response = view(request,pk=invoice.pk)
        self.assertEqual(response.status_code,201)
        self.assertEqual(response.data,{
            'single_value': '22.55', 
            'item': 'test Item', 
            'invoice': 4, 
            'quantity': 20, 
            'description': 'test Description',
            'line_total': 451.00,
        })
        

    #def test_logged_invoice_line_update(self):

    #def test_logged_invoice_line_delete(self):
    
    #def test_not_logged_invoice_and_line(self):

    #def test_totals_of_invoice_ok(self):

    #def test_line_total_ok(self):


   

    


class TaxRateTestCase(TestCase):
    def setup(self):
        pass

    #test_logged_create_tax_rate(self):

    #test_logged_update_tax_rate(self):

    #test_logged_delete_tax_rate(self):

    #test_logged_get_tax_rate_list(self):

    #test_logged_get_tax_rate(self):

       

