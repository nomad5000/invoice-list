# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('invoicelist', '0008_auto_20150927_0831'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invoiceline',
            name='invoice',
            field=models.ForeignKey(related_name='invoice_lines', to='invoicelist.Invoice'),
        ),
    ]
