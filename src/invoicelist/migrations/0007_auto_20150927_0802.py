# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('invoicelist', '0006_invoiceline'),
    ]

    operations = [
        migrations.CreateModel(
            name='Invoice',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('amount', models.DecimalField(decimal_places=2, max_digits=10)),
                ('due_date', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='TaxRate',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.CharField(max_length=20, unique=True)),
                ('rate', models.DecimalField(decimal_places=2, max_digits=2)),
            ],
        ),
        migrations.AddField(
            model_name='invoice',
            name='vat_tax_rate',
            field=models.ForeignKey(to='invoicelist.TaxRate'),
        ),
        migrations.AddField(
            model_name='invoiceline',
            name='invoice',
            field=models.ForeignKey(default=1, to='invoicelist.Invoice'),
            preserve_default=False,
        ),
    ]
