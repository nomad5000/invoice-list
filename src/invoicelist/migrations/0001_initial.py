# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='SiteConfiguration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('first_name', models.CharField(max_length=30)),
                ('last_name', models.CharField(max_length=30)),
                ('company', models.CharField(max_length=30)),
                ('address_street', models.CharField(max_length=30)),
                ('postalcode', models.CharField(max_length=8)),
                ('country', models.CharField(max_length=8)),
                ('vat_tax_id', models.CharField(max_length=8)),
                ('telephone', models.CharField(max_length=30)),
            ],
            options={
                'verbose_name': 'Site Configuration',
            },
        ),
    ]
