# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('invoicelist', '0009_auto_20150927_1235'),
    ]

    operations = [
        migrations.AddField(
            model_name='invoiceline',
            name='line_total',
            field=models.DecimalField(max_digits=10, default=10, decimal_places=2),
            preserve_default=False,
        ),
    ]
