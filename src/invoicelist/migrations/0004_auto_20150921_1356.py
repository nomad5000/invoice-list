# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('invoicelist', '0003_auto_20150921_1354'),
    ]

    operations = [
        migrations.AlterField(
            model_name='siteconfiguration',
            name='vat_tax_id',
            field=models.CharField(max_length=30),
        ),
    ]
