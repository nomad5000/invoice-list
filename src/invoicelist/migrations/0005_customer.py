# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('invoicelist', '0004_auto_20150921_1356'),
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=30)),
                ('last_name', models.CharField(max_length=30)),
                ('company', models.CharField(max_length=30)),
                ('address_street', models.CharField(max_length=30)),
                ('postalcode', models.CharField(max_length=8)),
                ('city', models.CharField(max_length=30)),
                ('country', models.CharField(max_length=8)),
                ('vat_tax_id', models.CharField(max_length=30)),
                ('phone_number', models.CharField(max_length=30)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
