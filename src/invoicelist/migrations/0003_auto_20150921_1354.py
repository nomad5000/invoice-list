# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('invoicelist', '0002_siteconfiguration_city'),
    ]

    operations = [
        migrations.RenameField(
            model_name='siteconfiguration',
            old_name='telephone',
            new_name='phone_number',
        ),
    ]
