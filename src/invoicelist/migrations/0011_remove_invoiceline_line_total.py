# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('invoicelist', '0010_invoiceline_line_total'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='invoiceline',
            name='line_total',
        ),
    ]
