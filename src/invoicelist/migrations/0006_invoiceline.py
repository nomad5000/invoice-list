# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('invoicelist', '0005_customer'),
    ]

    operations = [
        migrations.CreateModel(
            name='InvoiceLine',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('item', models.CharField(null=True, max_length=30, blank=True)),
                ('description', models.CharField(max_length=200)),
                ('quantity', models.IntegerField(null=True)),
                ('single_value', models.DecimalField(max_digits=10, decimal_places=2)),
            ],
        ),
    ]
