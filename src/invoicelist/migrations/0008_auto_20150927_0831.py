# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('invoicelist', '0007_auto_20150927_0802'),
    ]

    operations = [
        migrations.AddField(
            model_name='invoice',
            name='customer',
            field=models.ForeignKey(to='invoicelist.Customer', default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='invoice',
            name='invoice_date',
            field=models.DateField(default=datetime.datetime(2015, 9, 27, 8, 31, 46, 547518, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='invoice',
            name='invoice_number',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='invoice',
            name='invoice_number_prefix',
            field=models.CharField(null=True, blank=True, max_length=20),
        ),
    ]
