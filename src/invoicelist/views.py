from .models import Customer,SiteConfiguration,InvoiceLine,Invoice
from .serializers import SiteConfigurationSerializer,CustomerSerializer,InvoiceLineSerializer,InvoiceSerializer
from django.http import Http404
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status

class CustomerViewSet(viewsets.ModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer

class InvoiceViewSet(viewsets.ModelViewSet):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer

class InvoiceLineViewSet(viewsets.ModelViewSet):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceLineSerializer

class SiteConfigurationAPIView(APIView):
    """
    Retrieve or update the configuration
    """
    permission_classes = (IsAuthenticated,)
    def get_object(self):
        try:
            return SiteConfiguration.get_solo()
        except SiteConfiguration.DoesNotExist:
            raise Http404


    def get(self, request, format=None):
        config = self.get_object()
        serializer = SiteConfigurationSerializer(config)
        return Response(serializer.data)

    def put(self, request, format=None):
        config = self.get_object()
        serializer = SiteConfigurationSerializer(config, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
