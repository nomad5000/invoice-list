from django.db import models
from solo.models import SingletonModel


class Person(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    company = models.CharField(max_length=30)
    address_street = models.CharField(max_length=30)
    postalcode = models.CharField(max_length=8)
    city=models.CharField(max_length=30)
    country = models.CharField(max_length=8)
    vat_tax_id = models.CharField(max_length=30)
    phone_number = models.CharField(max_length=30)


    class Meta:
        abstract = True

class SiteConfiguration(SingletonModel,Person):

    def __unicode__(self):
        return u"Site Configuration"

    class Meta:
        verbose_name = "Site Configuration"

class Customer(Person):

    def __unicode__(self):
        return u"%s, %s"%(self.first_name,self.last_name)


class TaxRate(models.Model):
    name = models.CharField(max_length=20,unique=True)
    rate = models.DecimalField(max_digits=2, decimal_places=2) 

class Invoice(models.Model):
    invoice_number= models.IntegerField()
    invoice_number_prefix = models.CharField(max_length=20,blank=True,null=True)
    invoice_date = models.DateField()
    customer = models.ForeignKey('Customer')
    amount = models.DecimalField(max_digits=10,decimal_places=2)
    due_date= models.DateField()
    vat_tax_rate=models.ForeignKey('TaxRate')


class InvoiceLine(models.Model):
    item = models.CharField(max_length=30,blank=True,null=True)
    description = models.CharField(max_length=200)
    quantity = models.IntegerField(null=True)
    single_value = models.DecimalField(max_digits=10,decimal_places=2)
    invoice = models.ForeignKey('Invoice', related_name='invoice_lines')
    line_total = models.DecimalField(max_digits=10,decimal_places=2)

    def _get_line_total(self):
        return self.quantity * self.single_value
    line_total = property(_get_line_total)


